<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/style.css" rel="stylesheet" type="text/css">
		<title><?php echo $title; ?></title>
		<link rel="alternate" href="<?php echo $urlgem; ?>" type="text/gemini" title="Gemini protocol">
	</head>
	<body id="top">
		<?php if(count($tocs)>1)
		{
		?>
		<nav class="toc">
			<ul>
				<?php echo implode("\n",$tocs); ?>
			</ul>
		</nav>
		<?php
		}
		?>
		<main>
			<article>
			<?php echo implode("\n",$lines); ?>
			</article>
		</main>
		<div class="topanchor"><a href="#top">^</a></div>
		<footer>
		<p>
			<a href="https://validator.w3.org/check?uri=<?php echo urlencode($_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);?>"><img src="/html.png" alt="Valid HTML 5" height="31" width="88"></a>
	    </p>
		</footer>
		<header class="gemini">
			<span><?php echo $favicon; ?></span>
			<a href="<?php echo $urlgem; ?>" title="Gemini address"><?php echo htmlentities($urlgem); ?></a>
		</header>
	</body>
</html>
